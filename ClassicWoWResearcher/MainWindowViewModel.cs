﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using System.Security.Cryptography;
using System.Xml.Serialization;

namespace ClassicWoWResearcher
{
    public class MainWindowViewModel : INotifyPropertyChanged
    {
        //Async calls are used here for the conversion of ids to string names so that urls like wowwiki's can be used
        //When the user hits go an async call is sent off to wowdb and the resulting string is grabbed formt he title of the page
        readonly Dispatcher dispatcher = Dispatcher.CurrentDispatcher;
        private readonly BackgroundWorker worker = new BackgroundWorker();

        public MainWindowViewModel()
        {
            worker.DoWork += worker_DoWork;
            worker.RunWorkerCompleted += worker_RunWorkerCompleted;

            ReadInDefaults();
            OldSites.First(x => x.Name == "Granston's Guide").IsEnabled = false;
        }

        List<Site> defaults = new List<Site>();

        private string globalName = null;
        public string GlobalName { get { return globalName; } set { globalName = value; OnPropertyChanged("GlobalName"); } }

        private void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            Loading = Visibility.Visible;//make the spinner visible to show we've not crashed
            GlobalName = GetItemName();
            SelectedSearchTypeLabel = SelectedSearchType;
			if (SelectedSearchType == "Quest")
			{
				QuestInfo questInfo = DatabaseAccess.GetQuestLevelRange(Id);
				QuestLevel = questInfo.QuestLevel;
				QuestMinLevel = questInfo.MinLevel;
			}
		}

		private void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Loading = Visibility.Hidden;//hide the progress spinner, we're done now
            bool loadNamedPages = true;
            if (string.IsNullOrEmpty(globalName)) //this occurs if there was an error processing the title form the HTML. This can happen in some valid cases where wowdb does not have a specific quest. For this reason give the user the option to override
                if (
                    MessageBox.Show(
                        "Specified NPC/Quest/Item could not be found.\n\nAttempt to load web pages anyway?\n\nNote that web pages with a url based on the name of the entity will not be loaded.",
                        "404, not found", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.No)
                    return;
                else
                    loadNamedPages = false;

            IEnumerable<CheckBoxItem> oldSiteItems = loadNamedPages ? OldSites.Where(x => x.IsChecked) : OldSites.Where(x => !x.NameBasedUrl && x.IsChecked);
            IEnumerable<CheckBoxItem> privateSiteItems = loadNamedPages ? PrivateSites.Where(x => x.IsChecked) : PrivateSites.Where(x => !x.NameBasedUrl && x.IsChecked);

            //foreach both the old sites list and the private sites list, if they're checked then open a process which opens a new borser tab with the coresponding url
            foreach (CheckBoxItem checkBoxItem in oldSiteItems)
                Process.Start(checkBoxItem.GetUrl(SelectedSearchType, Id, globalName));

            foreach (CheckBoxItem checkBoxItem in privateSiteItems)
                Process.Start(checkBoxItem.GetUrl(SelectedSearchType, Id));
        }

        private Visibility loading = Visibility.Hidden;//spinner is hidden by default

        public Visibility Loading
        {
            get { return loading; }
            set
            {
                loading = value;
                OnPropertyChanged("Loading");
            }
        }

        private ICommand goCommand;
        public ICommand GoCommand
        {
            get { return goCommand ?? (goCommand = new RelayCommand(param => Go())); }
        }

        public void Go()
        {
            try
            {
                dispatcher.BeginInvoke(new Action(() => worker.RunWorkerAsync()));//invalid operation exception
            }
            catch (InvalidOperationException)
            {
                //I believe this occurs if another worker is attempted to be created while one is already working
                //So when the user clicks go twice, in this instance we want to do nothing, hence the empty catch
            }
        }

        private ICommand aboutCommand;
        public ICommand AboutCommand
        {
            get { return aboutCommand ?? (aboutCommand = new RelayCommand(param => About())); }
        }

        public void About()
        {
            About win2 = new About();
            win2.Show();
        }

        private ICommand allOldCommand;
        public ICommand AllOldCommand
        {
            get { return allOldCommand ?? (allOldCommand = new RelayCommand(param => AllOld())); }
        }

        //select all old sites
        public void AllOld()
        {
            foreach (CheckBoxItem checkBoxItem in OldSites.Where(checkBoxItem => checkBoxItem.IsEnabled))
                checkBoxItem.IsChecked = true;
        }

        private ICommand allPrivateCommand;
        public ICommand AllPrivateCommand
        {
            get { return allPrivateCommand ?? (allPrivateCommand = new RelayCommand(param => AllPrivate())); }
        }

        //select all private sites
        public void AllPrivate()
        {
            foreach (CheckBoxItem checkBoxItem in PrivateSites.Where(checkBoxItem => checkBoxItem.IsEnabled))
                checkBoxItem.IsChecked = true;
        }

        private ICommand clearCommand;
        public ICommand ClearCommand
        {
            get { return clearCommand ?? (clearCommand = new RelayCommand(param => Clear())); }
        }


        //clear all checkboxes
        public void Clear()
        {
            foreach (CheckBoxItem checkBoxItem in PrivateSites.Where(checkBoxItem => checkBoxItem.IsEnabled))
                checkBoxItem.IsChecked = false;

            foreach (CheckBoxItem checkBoxItem in OldSites.Where(checkBoxItem => checkBoxItem.IsEnabled))
                checkBoxItem.IsChecked = false;
        }

        private ICommand recommendedCommand;
        public ICommand RecommendedCommand
        {
            get { return recommendedCommand ?? (recommendedCommand = new RelayCommand(param => Recommended())); }
        }

        //select recommended sites
        public void Recommended()
        {
            Clear();
            foreach (CheckBoxItem checkBoxItem in OldSites.Where(checkBoxItem => checkBoxItem.Name == "Wowhead" || checkBoxItem.Name == "Allakhazam" || checkBoxItem.Name == "Thottbot" || checkBoxItem.Name == "GoblinWorkshop" || checkBoxItem.Name == "WowWiki" || checkBoxItem.Name == "World of war" || checkBoxItem.Name == "WoW Data"))
                checkBoxItem.IsChecked = true;
        }

        private ICommand loadDefaultsCommand;
        public ICommand LoadDefaultsCommand
        {
            get { return loadDefaultsCommand ?? (loadDefaultsCommand = new RelayCommand(param => LoadDefaults())); }
        }

        public void LoadDefaults()
        {
            foreach (Site site in defaults)
            {
                if (site.Catagory == "Old")
                    OldSites.First(x => x.Name == site.Name).IsChecked = site.Selected;
                else if (site.Catagory == "Private")
                    PrivateSites.First(x => x.Name == site.Name).IsChecked = site.Selected;
            }
        }

        private ICommand saveDefaultsCommand;
        public ICommand SaveDefaultsCommand
        {
            get { return saveDefaultsCommand ?? (saveDefaultsCommand = new RelayCommand(param => SaveDefaults())); }
        }

        public void SaveDefaults()
        {
            defaults = new List<Site>();

            foreach (CheckBoxItem checkBoxItem in OldSites)
                defaults.Add(new Site() { Name = checkBoxItem.Name, Selected = checkBoxItem.IsChecked, Catagory = "Old" });

            foreach (CheckBoxItem checkBoxItem in PrivateSites)
                defaults.Add(new Site() { Name = checkBoxItem.Name, Selected = checkBoxItem.IsChecked, Catagory = "Private" });

            WriteOutDefaults();
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propName));
        }

        //hold the list of all old sites
        private ObservableCollection<CheckBoxItem> oldSites = new ObservableCollection<CheckBoxItem>
                            {

                                new CheckBoxItem{ IsChecked = false, Name = "Wowhead", Url = "http://www.wowhead.com", NpcUrl = "/npc=", ItemUrl = "/item=", ObjectUrl = "/object=", QuestUrl = "/quest=", NameBasedUrl = false },

                                new CheckBoxItem{ IsChecked = false, Name = "WowDb", Url = "http://www.wowdb.com", NpcUrl = "/npcs/", ItemUrl = "/items/", ObjectUrl = "/objects/", QuestUrl = "/quests/", NameBasedUrl = false },

                                new CheckBoxItem{ IsChecked = false, Name = "Allakhazam", Url = "https://web.archive.org/web/*/http://wow.allakhazam.com", ItemUrl = "/db/item.html?witem=", NpcUrl = "/db/mob.html?wmob=", QuestUrl = "/db/quest.html?wquest=", ObjectUrl = "/db/object.html?wobject=", NameBasedUrl = false },

                                //note that thottbot links (i think only quest links) can take on two forms. the top one shown here appears to be the less common so we ignore it and use the other one
                                //https://web.archive.org/web/20090421234917/http://thottbot.com/?qu=7787
                                //https://web.archive.org/web/20100818064158/http://thottbot.com/q7787
                                new CheckBoxItem{ IsChecked = false, Name = "Thottbot", Url = "https://web.archive.org/web/*/http://www.thottbot.com", QuestUrl = "/q", ItemUrl = "/i", NpcUrl = "/c", ObjectUrl = "/o", NameBasedUrl = false },

                                new CheckBoxItem{ IsChecked = false, Name = "GoblinWorkshop", Url = "https://web.archive.org/web/*/http://www.goblinworkshop.com", ItemUrl = "/items/", QuestUrl = "/quests/", NpcUrl = "/creatures/", ObjectUrl = "/objects/", Suffix = ".html", NameBasedUrl = true },

                                new CheckBoxItem{ IsChecked = false, Name = "WowWiki", Url = "http://www.wowwiki.com", ItemUrl = "/", QuestUrl = "/Quest:", NpcUrl = "/", NameBasedUrl = true },

                                new CheckBoxItem{ IsChecked = false, Name = "Wowpedia", Url = "http://www.wowpedia.org", ItemUrl = "/", QuestUrl = "/Quest:", NpcUrl = "/", NameBasedUrl = true },

                                new CheckBoxItem{ IsChecked = false, Name = "Google (classic daterange)", Url = "http://www.google.com", Suffix = "+daterange:2453169-2454101", QuestUrl = "/search?q=", ItemUrl = "/search?q=", NpcUrl = "/search?q=", ObjectUrl = "/search?q=", NameBasedUrl = true },

                                new CheckBoxItem{ IsChecked = false, Name = "Google (pre-cataclysm daterange)", Url = "http://www.google.com", Suffix = "+daterange:2453169-2455480", QuestUrl = "/search?q=", ItemUrl = "/search?q=", NpcUrl = "/search?q=", ObjectUrl = "/search?q=", NameBasedUrl = true },

                                new CheckBoxItem{ IsChecked = false, Name = "World of war", Url = "https://web.archive.org/web/*/http://wwndata.worldofwar.net", QuestUrl = "/quest.php?id=", ItemUrl = "/item.php?id=", NpcUrl = "/mob.php?name=", ObjectUrl = "/mob.php?name=", NameBasedUrl = true },

                                //this one below could be used theoretically however it uses totally different ids
                                //new CheckBoxItem{ IsChecked = false, Name = "Gamepressure", Url = "http://wow.gamepressure.com", ItemUrl = "/item.asp?ID=", QuestUrl = "/quest.asp?ID=", NpcUrl = "/npc.asp?ID=", ObjectUrl = "/object.asp?ID=", NameBasedUrl = false },

                                new CheckBoxItem{ IsChecked = false, Name = "Magelo", Url = "http://wow.magelo.com", ItemUrl = "/en/item/", QuestUrl = "/en/quest/", NpcUrl = "/en/npc/", ObjectUrl = "/en/object/", NameBasedUrl = false },

                                new CheckBoxItem{ IsChecked = false, Name = "WoW Data", Url = "http://old.wowdata.ru", ItemUrl = "/item.html?id=", QuestUrl = "/quest.html?id=", NpcUrl = "/npc.html?id=", ObjectUrl = "/object.html?id=", NameBasedUrl = false },

                                new CheckBoxItem{ IsChecked = false, Name = "Granston's Guide", Url = "http://granstonsguide.com/", /*ItemUrl = "/item.html?id=", QuestUrl = "/quest.html?id=", NpcUrl = "/npc.html?id=", ObjectUrl = "/object.html?id=",*/ NameBasedUrl = true },

                                new CheckBoxItem{ IsChecked = false, Name = "Somepage", Url = "http://wow.somepage.com", ItemUrl = "/item/", QuestUrl = "/quest/", ObjectUrl = "/object/", NpcUrl = "/npc/", NameBasedUrl = false },

                                new CheckBoxItem{ IsChecked = false, Name = "WowPeak", Url = "http://www.wowpeek.com", ItemUrl = "/item.php?itemid=", NpcUrl = "/mob.php?mobid=", QuestUrl = "/quest.php?questid=", NameBasedUrl = false },

                                new CheckBoxItem{ IsChecked = false, Name = "shoot.tauri", Url = "http://shoot.tauri.hu", ItemUrl = "/?item=", ObjectUrl = "/?object=", NpcUrl = "/?npc=", QuestUrl = "/?quest=", NameBasedUrl = false }
                            };

        public ObservableCollection<CheckBoxItem> OldSites
        {
            get { return oldSites; } set { oldSites = value; OnPropertyChanged("OldSites"); }
        }

        //hold the list of all private sites
        private ObservableCollection<CheckBoxItem> privateSites = new ObservableCollection<CheckBoxItem>
                               {
                                   //working
                                   new CheckBoxItem {IsChecked = false, Name = "Valkyrie", Url = "http://db.valkyrie-wow.org", ItemUrl = "/?item=", ObjectUrl = "/?object=", NpcUrl = "/?npc=", QuestUrl = "/?quest=", NameBasedUrl = false },
                                   //working
                                   new CheckBoxItem {IsChecked = false, Name = "Vanilla Gaming", Url = "http://db.vanillagaming.org", ItemUrl = "/?item=", ObjectUrl = "/?object=", NpcUrl = "/?npc=", QuestUrl = "/?quest=", NameBasedUrl = false },
                                   //working
                                   new CheckBoxItem {IsChecked = false, Name = "Feenix", Url = "http://database.wow-one.com", ItemUrl = "/?item=", ObjectUrl = "/?object=", NpcUrl = "/?npc=", QuestUrl = "/?quest=", NameBasedUrl = false },
								   //working
								   new CheckBoxItem {IsChecked = false, Name = "Kronos", Url = "https://vanilla-twinhead.twinstar.cz", ItemUrl = "/?item=", ObjectUrl = "/?object=", NpcUrl = "/?npc=", QuestUrl = "/?quest=", NameBasedUrl = false }
							   };
        public ObservableCollection<CheckBoxItem> PrivateSites
        {
            get { return privateSites; } set { privateSites = value; OnPropertyChanged("PrivateSites"); }
        }

        private string selectedSearchType = "Item";
        public string SelectedSearchType
        {
            get
            {
                return selectedSearchType;
            }
            set
            {
                selectedSearchType = value;

                if(GlobalName == null)
                    SelectedSearchTypeLabel = value;
                //allakhazam uses different ids for objects, and goblin workshop doens't track objects at all, so disable both of those check boxes
                OldSites.First(x => x.Name == "GoblinWorkshop").IsEnabled = value != "Object";
                OldSites.First(x => x.Name == "Allakhazam").IsEnabled = value != "Object";
                OldSites.First(x => x.Name == "Granston's Guide").IsEnabled = (value == "NPC" || value == "Quest");

                if (value == "Object")
                {
                    OldSites.First(x => x.Name == "GoblinWorkshop").IsChecked = false;
                    OldSites.First(x => x.Name == "Allakhazam").IsChecked = false;
                }
                if(value == "Object" || value == "Item")
                    OldSites.First(x => x.Name == "Granston's Guide").IsChecked = false;

                OnPropertyChanged("SelectedSearchType");
            }
        }

        private string selectedSearchTypeLabel = "Item:";

        public string SelectedSearchTypeLabel
        {
            get
            {
                return selectedSearchTypeLabel;
            }
            set
            {
                selectedSearchTypeLabel = value + ":";
                OnPropertyChanged("SelectedSearchTypeLabel");
            }
        }

        //for the combo box
        private ObservableCollection<string> searchType = new ObservableCollection<string> { "Item", "Object", "NPC", "Quest" };
        public ObservableCollection<string> SearchType
        {
            get { return searchType; } set { searchType = value; OnPropertyChanged("SearchType"); }
        }

        private int id;
        public int Id
        {
            get { return id; } set { id = value; OnPropertyChanged("Id"); }
        }

        //Get the item name. Id is global since it has to be bound for wpf so we don't need to pass it in here as a perameter
        public string GetItemName()
        {
            string name = null;

            //here we want to use wowdb to convert an id to an item name
            //wowhead isn't used because it rejects the http get requiest for some reason

            //make a new refrence to the wowDb object otherwise we'll ahve to repeat the linq query a lot
            CheckBoxItem wowDb = OldSites.First(x => x.Name == "WowDb");
            string html = "";
            try
            {
                html = GetHtml(wowDb.GetUrl(SelectedSearchType, Id));
            }
            catch (WebException e)
            {
                return null;
            }
            name = Regex.Match(html, "<title>(?:\\[[A-Z]+\\])? ?(.+) - (?:.+) - (?:.+)</title>").Groups[1].ToString();
            name = WebUtility.HtmlDecode(name);//remove html encoding like %20

            name = Regex.Replace(name, "\\[.+\\] ?", "");//some have text like [DEPRECATED] in the name which ruins urls later on, remove it if found

            return name;
        }

		private int questMinLevel;
		public int QuestMinLevel
		{
			get { return questMinLevel; }
			set { questMinLevel = value; OnPropertyChanged("QuestMinLevel"); }
		}

		private int questLevel;
		public int QuestLevel
		{
			get { return questLevel; }
			set { questLevel = value; OnPropertyChanged("QuestLevel"); }
		}

		/// <summary>
		/// Returns html form a given url
		/// </summary>
		/// <param name="url">Full url to get website from</param>
		/// <returns>Resulting HTML as a string form that site</returns>
		public static string GetHtml(string url)
        {
            WebRequest request = WebRequest.Create(url);
            WebResponse response = request.GetResponse();
            Stream data = response.GetResponseStream();
            string html = String.Empty;
            using (StreamReader sr = new StreamReader(data))
            {
                html = sr.ReadToEnd();
            }
            return html;
        }

        private readonly Patches p = new Patches();

        private string searchDate;
        public string SearchDate
        {
            get { return searchDate; }
            set
            {
                searchDate = value;
                OnPropertyChanged("SearchDate");

                p.SetPatch(ValidateTime(value));
                Expansion = p.Expansion;
                Patch = p.Patch;
            }
        }

        private string expansion;
        public string Expansion { get { return expansion; } set { expansion = value; OnPropertyChanged("Expansion"); } }

        private string patch;

        public string Patch
        {
            get
            {
                if (!String.IsNullOrEmpty(patch))
                    return "Patch " + patch;
                return patch;
            }
            set { patch = value; OnPropertyChanged("Patch"); }
        }

        private DateTime ValidateTime(string date)
        {
            DateTime dt;
            return DateTime.TryParse(date, out dt) ? dt : DateTime.MaxValue;
        }

        /// <summary>
        /// Save out the current information in the zones list to an xml file
        /// </summary>
        public void WriteOutDefaults()
        {
            XmlSerializer xs = new XmlSerializer(typeof(List<Site>));
            using (FileStream fs = new FileStream("defaults.xml", FileMode.Create))
            {
                xs.Serialize(fs, defaults);
            }
        }

        /// <summary>
        /// Load information from the xml file into memory in the form of the zones list
        /// </summary>
        public void ReadInDefaults()
        {
            try
            {
                using (FileStream fs = new FileStream("defaults.xml", FileMode.Open))
                {
                    XmlSerializer xSer = new XmlSerializer(typeof(List<Site>));
                    defaults = (List<Site>)(xSer.Deserialize(fs));
                }
            }
            catch (FileNotFoundException e)
            {

            }
        }
    }

    public class Site
    {
        public string Catagory { get; set; }
        public string Name { get; set; }
        public bool Selected { get; set; }
    }

    public class CheckBoxItem : INotifyPropertyChanged
    {
        public string Url { get; set; }
        public string ObjectUrl { get; set; }
        public string ItemUrl { get; set; }
        public string NpcUrl { get; set; }
        public string QuestUrl { get; set; }
        public string Suffix { get; set; }
        public bool NameBasedUrl { get; set; }

        private bool isChecked;
        public bool IsChecked
        {
            get { return isChecked; }
            set
            {
                isChecked = value;
                NotifyPropertyChanged("IsChecked");
            }
        }

        private string name;
        public string Name
        {
            get { return name; }
            set
            {
                name = value;
                NotifyPropertyChanged("Name");
            }
        }

        private bool isEnabled = true;
        public bool IsEnabled { get { return isEnabled; } set { isEnabled = value; NotifyPropertyChanged("IsEnabled"); } }

        /// <summary>
        /// Given a copy of the selected search type form the combo box and an id, this will return the full url to use for invoking a web browser.
        /// If the page url depends on the name of the item rather than an id, then set id to null and put the name straight from the wowdb page into nameOfItem
        /// </summary>
        /// <param name="selectedSearchType"></param>
        /// <param name="id"></param>
        /// <param name="nameOfItem"></param>
        /// <returns></returns>
        public string GetUrl(string selectedSearchType, int? id, string nameOfItem = null)
        {
            if (!NameBasedUrl)
            {
                switch (selectedSearchType)
                {
                    case "Item":
                        return Url + ItemUrl + id + Suffix;
                    case "Object":
                        return Url + ObjectUrl + id + Suffix;
                    case "NPC":
                        return Url + NpcUrl + id + Suffix;
                    case "Quest":
                        return Url + QuestUrl + id + Suffix;
                }
            }
            else if(nameOfItem != null)//check not really needed here as it should never fail but it prevents the posability of null ref exceptions in the switch
            {
                //replace characters specific for goblin workshop, no commas or apostrophes, spaces are replaced by hyphen
                if (name == "GoblinWorkshop")
                {
                    nameOfItem = nameOfItem.Replace("'", "");
                    nameOfItem = nameOfItem.Replace(",", "");
                    nameOfItem = nameOfItem.Replace(" ", "-");
                }
                else if (name.StartsWith("Google"))
                {
                    nameOfItem = nameOfItem.Replace(" ", "+");
                }
                else if (name == "World of war")//this site uses ids for items but names for mobs and objects, for quests it uses the md5 hash of the quest id
                {
                    switch (selectedSearchType)
                    {
                        case "Item":
                            nameOfItem = id.ToString();
                            break;
                        case "Object":
                        case "NPC":
                            nameOfItem = nameOfItem.Replace(" ", "+");
                            break;
                        case "Quest":
                            nameOfItem = GetMd5Hash(id.ToString());
                            break;
                    }
                }
                else if (name == "Granston's Guide")
                {
                    if (selectedSearchType == "NPC")
                    {
                        nameOfItem = ZoneLookupTable.GetGranstonUrl(nameOfItem);
                    }
                    else if (selectedSearchType == "Quest")
                    {
                        List<GranstonQuest> quests = QuestZoneLookupTable.GetQuests(nameOfItem);
                        //if (quests.Count > 1)
                        //{
                        //    for (int i = 0; i < quests.Count - 1; i++)
                        //    {
                        //        Process.Start(Url + quests[i].Url);
                        //    }
                        //    nameOfItem = quests[quests.Count - 1].Url;
                        //}
                        //else
                            nameOfItem = quests[0].Url;
                    }
                }
                else//everything else uses underscores for spaces
                {
                    nameOfItem = nameOfItem.Replace(' ', '_');
                }
                switch (selectedSearchType)
                {
                    case "Item":
                        return Url + ItemUrl + nameOfItem + Suffix;
                    case "Object":
                        return Url + ObjectUrl + nameOfItem + Suffix;
                    case "NPC":
                        return Url + NpcUrl + nameOfItem + Suffix;
                    case "Quest":
                        return Url + QuestUrl + nameOfItem + Suffix;
                }
            }

            return "";
        }

        public static string GetMd5Hash(string input)
        {
            MD5 md5Hash = MD5.Create();
            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void NotifyPropertyChanged(string strPropertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(strPropertyName));
        }
    }
}
