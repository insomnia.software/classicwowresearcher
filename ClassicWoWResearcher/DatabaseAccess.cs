﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using MySql.Data.MySqlClient;

namespace ClassicWoWResearcher
{
	class DatabaseAccess
	{
		public static QuestInfo GetQuestLevelRange(int questID)
		{
			MySqlConnection conn;

			try
			{
				conn = new MySqlConnection("redacted");
				conn.Open();

			}
			catch (MySqlException ex)
			{
				MessageBox.Show("Error: {0}", ex.ToString());
				return null;
			}

			string query = $"select minlevel, questlevel from quest_template where entry={questID};";

			MySqlCommand command = new MySqlCommand(query, conn);
			MySqlDataReader dataReader = command.ExecuteReader();

			QuestInfo questInfo = null;

			while (dataReader.Read())
			{
				questInfo = new QuestInfo
				{
					MinLevel = dataReader.GetInt32(0),
					QuestLevel = dataReader.GetInt32(1)
				};
			}

			dataReader.Close();
			conn.Close();

			return questInfo;
		}

		public static bool IsQuestTextTheSame(int questID)
		{
			return false;
		}
	}

	public class QuestInfo
	{
		public int MinLevel { get; set; }
		public int QuestLevel { get; set; }
	}
}
