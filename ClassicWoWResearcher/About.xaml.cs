﻿using System.Windows;

namespace ClassicWoWResearcher
{
    /// <summary>
    /// Interaction logic for About.xaml
    /// </summary>
    public partial class About : Window
    {
        public About()
        {
            InitializeComponent();
            DataContext = new AboutViewModel();
        }
    }
}
