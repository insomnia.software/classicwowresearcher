﻿using System;
using System.ComponentModel;
using System.IO;
using System.Net;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Threading;

namespace ClassicWoWResearcher
{
    class AboutViewModel : INotifyPropertyChanged
    {
        readonly Dispatcher dispatcher = Dispatcher.CurrentDispatcher;
        private readonly BackgroundWorker worker = new BackgroundWorker();

        public AboutViewModel()
        {
            worker.DoWork += worker_DoWork;

            try
            {
                dispatcher.BeginInvoke(new Action(() => worker.RunWorkerAsync()));//invalid operation exception
            }
            catch (InvalidOperationException)
            {
                //I believe this occurs if another worker is attempted to be created while one is already working
                //So when the user clicks go twice, in this instance we want to do nothing, hence the empty catch
            }
        }

        private void worker_DoWork(object sender, DoWorkEventArgs e)
        {
	        return;
            WebRequest request = WebRequest.Create("redacted");
            WebResponse response = request.GetResponse();
            Stream data = response.GetResponseStream();
            using (StreamReader sr = new StreamReader(data))
            {
                LatestVersion = sr.ReadToEnd();
            }
        }

        private string latestVersion = "Checking...";
        public string LatestVersion { get { return latestVersion; } set { latestVersion = value; OnPropertyChanged("LatestVersion"); } }

        private string aboutText = Regex.Match(Assembly.GetExecutingAssembly().ToString(), "[0-9].[0-9].[0-9].[0-9]").ToString().Replace("=", ": ");
        public string AboutText { get { return aboutText; } set { aboutText = value; OnPropertyChanged("AboutText"); } }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propName));
        }
    }
}
